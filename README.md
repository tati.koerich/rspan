# Reading Span (RSPAN) Task
The reading span (RSPAN) task as a Web application.  This task estimates the size of the working memory capacity.


## Launching
There are two ways of launching the task.  To launch it off the internet, simply navigate to:
[https://ubiq-x.gitlab.io/rspan](https://ubiq-x.gitlab.io/rspan/)

If you don't have internet access on your experimental machine or simply prefer to run in localy, clone this repository:
```
git clone https://gitlab.com/ubiq-x/rspan
```
and open the ```index.html``` in your browser.

Either way, you will want to enter full-screen mode in your browser to get best user experience.


## References
Conway, A.R.A., Kane, M.J., Bunting, M.F., Hambrick, D.Z., Wilhelm, O., Engle, R.W. (2005) Working memory span tasks: A methodological review and user's guide. _Psychonomic Bulletin & Review_, 12, 769-786.


## Citing
Loboda, T.D. (2012).  Reading Span (RSPAN) Task [Web application].  Available at: _https://ubiq-x.gitlab.io/rspan_


## License
This project is licensed under the [BSD License](LICENSE.md).
